from typing import List
import datetime
from carte import Carte
from chat import Chat
from deck import Deck
from joueur import Joueur
from historique_partie import HistoriquePartie
from coinche import PartieCoinche

def get_ind_joueur(liste_joueur :List[Joueur],joueur : Joueur):
    for i in range (4):
        if(liste_joueur[i]==joueur):
            return i


if __name__ == '__main__':
    joueur1 = Joueur(1)
    joueur2 = Joueur(2)
    joueur3 = Joueur(3)
    joueur4 = Joueur(4)

    liste_joueur = []
    liste_joueur.append(joueur1)
    liste_joueur.append(joueur2)
    liste_joueur.append(joueur3)
    liste_joueur.append(joueur4)

    partie = PartieCoinche()
    partie_4j = partie.creer_partie_4_joueur(liste_joueur)
    partie_4j.lancer_partie()
    partie_4j.distribuer()

    cpt = 0
    while partie_4j.get_statut() == "lancement":
        if cpt % 4 == 0:
            e = partie_4j._donneur_index + 1 % 4
        bool = False
        while bool == False:
            try:
                print("tour du joueur" + str(e%4))
                for carte in liste_joueur[e%4].cartes:
                    print(str(carte))
                val = int(input("\033[0;33m donner montant enchere ( 0 pour passer ) : \033[0m"))
                atout = str(input("\033[0;33m donner atout, coeur, trefle, pique, carreau, passer pour passer : \033[0m"))
                partie_4j.annoncer_enchere(liste_joueur[e%4], val , atout)
                bool = True
            except Exception:
                print("\033[0;31m Enchere choisi pas valide, Veuillez Rentrer une autre enchere \033[0m")
        e+=1
        cpt+=1


    joueur_commence_jeu = liste_joueur[(partie_4j._donneur_index + 1) % 4]


    for i in range (8):         # TODO : afficher etat de la partie pour savoir ou on en est
        tour_joueur = joueur_commence_jeu
        ind_joueur = get_ind_joueur(liste_joueur, tour_joueur)
        for i in range (4):
            ind_actu = ind_joueur%4
            joueur_jouer = liste_joueur[ind_actu]
            bool = False
            while bool == False:
                try:
                    print("joueur n : " + str(ind_actu))
                    v=0
                    for carte in joueur_jouer.cartes:
                        print("ind : " + str(v) + " carte : " + str(carte))
                        v+=1
                    ind_carte = int(input("\033[0;33m Sélectionnez l'indice de la carte à jouer: \033[0m"))
                    partie_4j.jouer_carte(joueur_jouer, joueur_jouer.cartes[ind_carte])
                    bool=True
                except Exception:
                    print("\033[0;31m carte choisi pas valide \033[0m")


            ind_joueur+=1

        gagnant_pli = partie_4j.terminer_pli(atout)
        joueur_commence_jeu = gagnant_pli

    histo = partie_4j.terminer_partie()
    print(histo)

