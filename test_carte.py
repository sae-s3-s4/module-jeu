
import unittest
from carte import Carte


class CarteTestCase(unittest.TestCase):

    def test_valeur(self):
        # as
        c = Carte("coeur", 1)
        self.assertEqual(c.valeur("coeur"), 11)
        self.assertEqual(c.valeur("pique"), 11)
        self.assertEqual(c.valeur("tout_atout"), 7)
        self.assertEqual(c.valeur("sans_atout"),19)

        # roi
        c = Carte("coeur", 13)
        self.assertEqual(c.valeur("coeur"), 4)
        self.assertEqual(c.valeur("pique"), 4)
        self.assertEqual(c.valeur("tout_atout"), 3)
        self.assertEqual(c.valeur("sans_atout"), 4)

        # dame
        c = Carte("coeur", 12)
        self.assertEqual(c.valeur("coeur"), 3)
        self.assertEqual(c.valeur("pique"), 3)
        self.assertEqual(c.valeur("tout_atout"), 2)
        self.assertEqual(c.valeur("sans_atout"), 3)

        # valet atout = 20, non atout = 2
        c = Carte("coeur", 11)
        self.assertEqual(c.valeur("coeur"), 20)
        self.assertEqual(c.valeur("pique"), 2)
        self.assertEqual(c.valeur("tout_atout"), 14)
        self.assertEqual(c.valeur("sans_atout"), 2)

        # 10
        c = Carte("coeur", 10)
        self.assertEqual(c.valeur("coeur"), 10)
        self.assertEqual(c.valeur("pique"), 10)
        self.assertEqual(c.valeur("tout_atout"), 5)
        self.assertEqual(c.valeur("sans_atout"), 10)

        # 9, atout = 14, non atout = 0
        c = Carte("coeur", 9)
        self.assertEqual(c.valeur("coeur"), 14)
        self.assertEqual(c.valeur("pique"), 0)
        self.assertEqual(c.valeur("tout_atout"), 9)
        self.assertEqual(c.valeur("sans_atout"), 0)

        # 8
        c = Carte("coeur", 8)
        self.assertEqual(c.valeur("coeur"), 0)
        self.assertEqual(c.valeur("pique"), 0)
        self.assertEqual(c.valeur("tout_atout"), 0)
        self.assertEqual(c.valeur("sans_atout"), 0)

        # 7
        c = Carte("coeur", 7)
        self.assertEqual(c.valeur("coeur"), 0)
        self.assertEqual(c.valeur("pique"), 0)
        self.assertEqual(c.valeur("tout_atout"), 0)
        self.assertEqual(c.valeur("sans_atout"), 0)

    def test_superieur(self):
        """
        Teste si une carte est superieur a une autre
        Si on compare un atout et un non atout, l'atout est superieur
        Si on compare deux cartes de la meme famille, la plus forte est superieur
        """
        c1 = Carte("coeur", 1)
        c2 = Carte("coeur", 13)
        self.assertTrue(c1.superieur(c2, "coeur"))
        self.assertFalse(c2.superieur(c1, "coeur"))

        # atout toujours superieur
        for i in range(1, 13) :
            c1 = Carte("coeur", i)
            for j in range(1, 13):
                c2 = Carte("pique", j)
                self.assertTrue(c1.superieur(c2, "coeur"))
                self.assertFalse(c2.superieur(c1, "coeur"))


    def test_superieur_tout_atout(self):
        c1 = Carte("coeur", 1)
        c2 = Carte("coeur", 13)
        self.assertTrue(c1.superieur(c2, "tout_atout", "coeur"))
        self.assertFalse(c2.superieur(c1, "tout_atout", "coeur"))

        # quand meme carte en tout atout on doit préciser la carte d'atout actuelle
        c1 = Carte("coeur", 11)
        c2 = Carte("pique", 11)
        # cas ou du coeur est atout
        self.assertTrue(c1.superieur(c2, "tout_atout", "coeur"))
        self.assertFalse(c2.superieur(c1, "tout_atout", "coeur"))
        # cas ou du pique est atout
        self.assertFalse(c1.superieur(c2, "tout_atout", "pique"))
        self.assertTrue(c2.superieur(c1, "tout_atout", "pique"))

        c1 = Carte("coeur", 7)
        c2 = Carte("pique", 1)
        # cas ou du coeur est atout
        self.assertTrue(c1.superieur(c2, "tout_atout", "coeur"))
        self.assertFalse(c2.superieur(c1, "tout_atout", "coeur"))
        # cas ou du pique est atout
        self.assertFalse(c1.superieur(c2, "tout_atout", "pique"))
        self.assertTrue(c2.superieur(c1, "tout_atout", "pique"))

        c1 = Carte("coeur", 7)
        c2 = Carte("coeur", 8)
        # cas ou du coeur est atout
        self.assertFalse(c1.superieur(c2, "tout_atout", "coeur"))
        self.assertTrue(c2.superieur(c1, "tout_atout", "coeur"))

        # On ne tombe jamais dans ce cas là car on compare toujours la carte la plus forte avec celle qu'on vient de poser
        # c1 = Carte("coeur", 11)
        # c2 = Carte("pique", 10)
        # # cas ou du trefle est atout
        # self.assertTrue(c1.superieur(c2, "tout_atout", "trefle"))
        # self.assertFalse(c2.superieur(c1, "tout_atout", "trefle"))




if __name__ == '__main__':
    unittest.main()
