from typing import List
import datetime

from carte import Carte
from chat import Chat
from deck import Deck
from joueur import Joueur

from historique_partie import HistoriquePartie

list_partie = []
"""
list_partie.append(PartieCoinche(123, "oui"))
list_partie.append(PartieCoinche(456, "non"))
liste_partie.append(PartieCoinche(789, "pouce"))
"""

class PartieCoinche:
    id: int
    code: str
    _deck: Deck
    _joueurs: List[Joueur]
    _statut: str
    _type: str
    _messages: List[Chat]
    _dernier_pli: List[Carte]
    _pli_actuelle: {}
    _carte_gagnante: Carte
    _donneur_index: int
    _pts_equipe_1: int
    _pts_equipe_2: int
    _enchere: int
    _atout_actuelle: str
    _equipe1: List[Joueur]
    _equipe2: List[Joueur]
    _dico_enchere: {}

    def __init__(self):
        self._joueurs = []
        self._deck = Deck()
        self._statut = "attente"
        self._type = "public"
        self._messages = []
        self._dernier_pli = []
        self._carte_gagnante = None
        self._pli_actuelle = {}
        self._enchere=0
        self._atout_actuelle = None
        self._pts_equipe_1 = 0
        self._pts_equipe_2 = 0
        self._equipe1 = []
        self._equipe2 = []
        self._dico_enchere = {}
        self._donneur_index = -1
    @staticmethod
    def partie_existe(code):
        for partie in list_partie:
            if(partie.code == code):
                return True
            else:
                return False

    def ajouter_joueur(self, joueur: Joueur):
        if(len(self._joueurs) < 4):
            self._joueurs.append(joueur)
        else:
            raise Exception("Partie déjà complète")

    def partie_complete(self):
        if (len(self._joueurs) == 4):
            return True
        else:
            return False

    def get_statut(self):
        return self._statut

    @staticmethod
    def creer_partie_4_joueur(joueur: List[Joueur]):
        newPartie = PartieCoinche()
        newPartie._joueurs=joueur
        list_partie.append(newPartie)
        return newPartie


    @staticmethod
    def creer_partie_prive():
        new_prv_partie = PartieCoinche()
        new_prv_partie._type = "privé"
        list_partie.append(new_prv_partie)
        return new_prv_partie

    def lancer_partie(self):
        self._statut = "lancement"
        # creation des equipes
        self._equipe1.append(self._joueurs[0])
        self._equipe1.append(self._joueurs[2])
        self._equipe2.append(self._joueurs[1])
        self._equipe2.append(self._joueurs[3])


    def distribuer(self):
        self._donneur_index = (self._donneur_index + 1 ) % 4    # MAJ de l'index du joueur qui distribue pour chaque partie, premier joueur qui distribue : 0
        self._deck.couper() # coupe le deck avant chaque distribution
        # Distribuer 3 cartes à chaque joueur, puis 2, puis encore 3
        for _ in range(3):
            for joueur in self._joueurs:
                joueur.cartes.append(self._deck.distribuer())
        for _ in range(2):
            for joueur in self._joueurs:
                joueur.cartes.append(self._deck.distribuer())
        for _ in range(3):
            for joueur in self._joueurs:
                joueur.cartes.append(self._deck.distribuer())
    def jouer_carte(self, joueur: Joueur, carte: Carte):
        """
        faire en sorte de stocker pour chaque carte le joueur qui l'a joué
        :param joueur:
        :param carte:
        :return:
        """
        if(self._verifier_coup(joueur, carte, self._atout_actuelle)):
            joueur.cartes.remove(carte)
            if joueur in self._pli_actuelle:
                print(" pas possible de jouer plusieurs fois")
            else:
                self._pli_actuelle[joueur] = carte
        else:
            raise Exception("Pas autorisé à jouer cette carte")


    def meilleur_carte_pli(self, liste_cartes: List[Carte], atout: str):
        """
        :param liste_cartes: liste de cartes du pli actuel
        :return: la meilleure carte de la liste
        """
        if(len(liste_cartes) == 1):
            return liste_cartes[0]
        best_carte = liste_cartes[0]
        for carte in liste_cartes[1:]:
            if carte.superieur(best_carte, atout, liste_cartes[0].get_famille()):
                best_carte = carte
        return best_carte

    def terminer_pli(self, atout: str):
        """
        Faire comparaison des cartes stockées dans la liste pli_actuelle
        Mettre pli_actuelle dans dernier_pli puis reset pli_actuelle
        :return: le joueur qui a gagné le pli
        """
        list_carte_pli = []
        del self._pli_actuelle["couleur"]
        for carte in self._pli_actuelle.values():
            list_carte_pli.append(carte)
        carte_gagnante = self.meilleur_carte_pli(list_carte_pli, atout)
        joueur_gagnant = list(self._pli_actuelle.keys())[list(self._pli_actuelle.values()).index(carte_gagnante)]
        self._dernier_pli = list_carte_pli

        # calcul des points de chaque equipe pour ce pli
        somme_points_equipe1_pli = 0
        for joueur in self._equipe1:
            somme_points_equipe1_pli += self._pli_actuelle.get(joueur).valeur(atout)
        somme_points_equipe2_pli = 0
        for joueur in self._equipe2:
            somme_points_equipe2_pli += self._pli_actuelle.get(joueur).valeur(atout)
        # Maj des scores de chaque equipe
        self._pts_equipe_1 += somme_points_equipe1_pli
        self._pts_equipe_2 += somme_points_equipe2_pli

        self._pli_actuelle = {}
        return joueur_gagnant

    def annoncer_enchere(self, joueur: Joueur, enchere, atout: str):
        if (atout == "passer" or enchere == 0 ):
            self._dico_enchere[joueur] = "passer"
        elif self._verifier_enchere(joueur, enchere):
            self._dico_enchere[joueur] = enchere
            self._enchere = enchere
            self._atout_actuelle = atout
        else:
            raise Exception("Enchere Pas Valide")
        count_joueurs_passe = 0
        count_joueurs_enchere = 0
        for joueur in self._dico_enchere.keys():
            if(self._dico_enchere[joueur] == "passer"):
                count_joueurs_passe+=1
            if(self._dico_enchere[joueur] != "passer"):
                count_joueurs_enchere +=1
        if count_joueurs_passe == 3 and count_joueurs_enchere == 1 :
            self._statut = "JEU"
        if count_joueurs_passe >= 4 :
            self._deck = Deck()
            for joueur in self._joueurs:
                joueur.cartes = []
            self.distribuer()


    def _verifier_enchere(self, joueur: Joueur, enchere):
        """
        Vérifie si l'enchère est valide
        :param joueur:
        :param enchere:
        :return: True or False
        """
        valid = False
        if(enchere >= 80 and enchere <= 500):
            if(enchere >= self._enchere+10):
                if(enchere%10 == 0):
                    valid = True
        return valid

    def _verifier_coup(self, joueur: Joueur, carte: Carte, atout: str):
        """
        Vérifie si le coup est valide
        :param joueur:
        :param carte:
        :return:
        """
        # TODO : faire que l'on ne puisse pas pisser à l'atout
        # verif qu'un joueur ne puisse pas jouer deux fois dans le meme pli
        for key in list(self._pli_actuelle.keys()):
            if key == joueur:
                return False
        if(carte not in joueur.cartes):   # si le joueur joue une carte qu'il ne possede pas
            return False
        if(self._pli_actuelle == {}): # si c'est la premiere carte a etre joué
            self._pli_actuelle["couleur"] = carte.get_famille() #stocke la premiere couleur joué du pli
            return True
        if(self._pli_actuelle.get("couleur")==carte.get_famille() and self._pli_actuelle.get("couleur") != atout): # si c'est la meme couleur que la premiere carte joué
            return True
        if(self._pli_actuelle.get("couleur")!=carte.get_famille()):
            for carte_ in joueur.cartes:                 # si c'est pas la meme couleur que la 1er -> verif que le joueur n'ait pas la couleur demandé
                if(carte_.get_famille()==self._pli_actuelle.get("couleur")):
                    return False
            return True
        if(carte.get_famille()==atout and self._pli_actuelle.get("couleur")==atout and self._pli_actuelle.get("couleur")==carte.get_famille()):   # si c'est de l'atout demandé et que le joueur joue de l'atout
            # recuperation de la liste des cartes actuelles pour test
            list_carte_pli = []
            couleur =  self._pli_actuelle.get("couleur")      #peut etre transformer cette partie en fonction a part
            del self._pli_actuelle["couleur"]
            for carte_ in self._pli_actuelle.values():
                list_carte_pli.append(carte_)
            self._pli_actuelle["couleur"] = couleur
            if(self.meilleur_carte_pli(list_carte_pli, atout).superieur(carte, atout, self._pli_actuelle.get("couleur") )): # verifie que la meilleur carte du pli (full atout) est superieur à la carte joué
                for carte_ in joueur.cartes:                         # si c'est le cas -> test que le joueur n'ait pas de carte dans son deck meilleur que la meilleur carte du pli actuelle
                    if(carte_.superieur(self.meilleur_carte_pli(list_carte_pli, atout), atout, self._pli_actuelle.get("couleur")) and carte_ != carte): # obliger de monter à l'atout donc doit jouer une carte au dessus de la meilleur si il le peut
                        return False
                return True
            if(carte.superieur(self.meilleur_carte_pli(list_carte_pli, atout), atout, self._pli_actuelle.get("couleur"))):
                return True

    def dernier_pli(self):
        """
        Retourne le dernier pli joué
        :return: List[Carte]
        """
        return self._dernier_pli

    def terminer_partie(self):
        """
        Créer un historique de partie
        :return: Historique de Partie
        """
        date = datetime.date.today()
        if(self._pts_equipe_1 > self._pts_equipe_2):
            gagnant1= self._equipe1[0]
            gagnant2= self._equipe1[1]
            perdant1 = self._equipe2[0]
            perdant2 = self._equipe2[1]
            nb_pts_gagnants = self._pts_equipe_1
            nb_pts_perdants = self._pts_equipe_2
        elif(self._pts_equipe_1 < self._pts_equipe_2):
            gagnant1 = self._equipe2[0]
            gagnant2 = self._equipe2[1]
            perdant1 = self._equipe1[0]
            perdant2 = self._equipe1[1]
            nb_pts_gagnants = self._pts_equipe_2
            nb_pts_perdants = self._pts_equipe_1
        else:
            gagnant1 = None
            gagnant2 = None
            perdant1 = None
            perdant2 = None
            nb_pts_perdants = self._pts_equipe_2
            nb_pts_gagnants = self._pts_equipe_1
        historique = HistoriquePartie(date, gagnant1, gagnant2, perdant1, perdant2, nb_pts_gagnants, nb_pts_perdants)
        return historique