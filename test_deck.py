import unittest

from carte import Carte
from deck import Deck

class MyTestCase(unittest.TestCase):
    def test_creation(self):
        d = Deck()
        print(d)
        self.assertEqual(len(d), 32)

    def test_couper(self):
        d = Deck()
        d.couper()
        self.assertEqual(len(d), 32)

    def test_distribuer(self):
        d = Deck()
        c = d.distribuer()
        self.assertEqual(len(d), 31)
        self.assertEqual(type(c), Carte)

    def test_set_cartes(self):
        d = Deck()
        d.set_cartes([Carte("coeur", 1) for i in range(32)])
        self.assertEqual(len(d), 32)
        self.assertEqual(d.distribuer().valeur("coeur"), 11)

        # il faut 32 cartes
        self.assertRaises(Exception, d.set_cartes, [Carte("coeur", 1)])




if __name__ == '__main__':
    unittest.main()
