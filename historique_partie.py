from datetime import datetime


class HistoriquePartie:
    date: datetime
    gagnants_1: str
    gagnants_2: str
    perdants_1: str
    perdants_2: str
    nb_points_gagnants: int
    nb_points_perdants: int

    def __init__(self, date, gagnants_1, gagnants_2, perdants_1, perdants_2, nb_points_gagnants, nb_points_perdants):
        self.date = date
        self.gagnants_1 = gagnants_1
        self.gagnants_2 = gagnants_2
        self.perdants_1 = perdants_1
        self.perdants_2 = perdants_2
        self.nb_points_gagnants = nb_points_gagnants
        self.nb_points_perdants = nb_points_perdants

    def __str__(self):
        return "|| date :" + str(self.getDate()) + "|| gagnant 1 : " + str(self.getGagnants1()) + "|| gagnant 2 : " + str(self.getGagnants2()) + "|| perdants 1 : " + str(self.getPerdants1()) + "|| perdants 2 : " + str(self.getPerdants2()) + "|| nb points gagnant: " +str(self.getNbPointsGagnants()) + "|| nb pts perdant : " + str(self.getNbPointsPerdants())

    def getDate(self):
        return self.date
    def getGagnants1(self):
        return self.gagnants_1
    def getGagnants2(self):
        return self.gagnants_2
    def getPerdants1(self):
        return self.perdants_1
    def getPerdants2(self):
        return self.perdants_2
    def getNbPointsGagnants(self):
        return self.nb_points_gagnants
    def getNbPointsPerdants(self):
        return self.nb_points_perdants