import unittest
from datetime import date

from coinche import PartieCoinche
from historique_partie import HistoriquePartie
from joueur import Joueur
from carte import Carte
class TestCoinche(unittest.TestCase):

    def setUp(self):
        self.joueurs = [Joueur(1), Joueur(2), Joueur(3), Joueur(4)]
        self.partie = PartieCoinche()

    def test_ajouter_joueur(self):
        for joueur in self.joueurs:
            self.partie.ajouter_joueur(joueur)
        self.assertEqual(len(self.partie._joueurs), 4)

        # Teste l'exception lorsque la partie est déjà complète
        with self.assertRaises(Exception):
            self.partie.ajouter_joueur(Joueur())

    def test_partie_complete(self):
        self.assertFalse(self.partie.partie_complete())
        for joueur in self.joueurs:
            self.partie.ajouter_joueur(joueur)
        self.assertTrue(self.partie.partie_complete())

    def test_creer_partie_4_joueurs(self):
        nouvelle_partie = PartieCoinche.creer_partie_4_joueur(self.joueurs)
        self.assertEqual(len(nouvelle_partie._joueurs), 4)

    def test_creer_partie_prive(self):
        nouvelle_partie_privee = PartieCoinche.creer_partie_prive()
        self.assertEqual(nouvelle_partie_privee._type, "privé")

    def test_lancer_partie(self):
        self.assertEqual(self.partie.get_statut(), "attente")
        self.partie._joueurs = self.joueurs
        self.partie.lancer_partie()
        self.assertEqual(self.partie.get_statut(), "lancement")

    def test_distribuer(self):
        self.partie.ajouter_joueur(self.joueurs[0])
        self.partie.ajouter_joueur(self.joueurs[1])
        self.partie.ajouter_joueur(self.joueurs[2])
        self.partie.ajouter_joueur(self.joueurs[3])

        self.partie.distribuer()
        for joueur in self.partie._joueurs:
            self.assertEqual(len(joueur.cartes), 8)

    def test_jouer_carte(self):
        self.partie.ajouter_joueur(self.joueurs[0])
        self.partie.distribuer()
        carte = self.joueurs[0].cartes[0]
        self.partie.annoncer_enchere(self.joueurs[0], 100, "coeur")

        self.partie.jouer_carte(self.joueurs[0], carte)
        self.assertEqual(len(self.joueurs[0].cartes), 7)

        # Vérifie que la carte est dans le pli actuel
        self.assertTrue(self.joueurs[0] in self.partie._pli_actuelle)
        self.assertEqual(self.partie._pli_actuelle[self.joueurs[0]], carte)

    def test_meilleur_carte_pli(self):
        carte1 = Carte("coeur", 10)
        carte2 = Carte("coeur", 9)
        carte3 = Carte("coeur", 8)
        carte4 = Carte("pique", 1)
        liste_cartes = [carte1, carte2, carte3, carte4]

        meilleure_carte = self.partie.meilleur_carte_pli(liste_cartes, "coeur")
        self.assertEqual(meilleure_carte, carte2)
        meilleure_carte = self.partie.meilleur_carte_pli(liste_cartes, "pique")
        self.assertEqual(meilleure_carte, carte4)

    def test_terminer_pli(self):
        self.partie.ajouter_joueur(self.joueurs[0])
        self.partie.ajouter_joueur(self.joueurs[1])
        self.partie.ajouter_joueur(self.joueurs[2])
        self.partie.ajouter_joueur(self.joueurs[3])
        self.partie.distribuer()
        for joueur in self.joueurs:
            i = 7
            joueur.cartes[0] = Carte("pique", i)
            i+=1
        # Assurez-vous que chaque joueur a au moins une carte avant de jouer
        for joueur in self.joueurs:
            if joueur.cartes != None:
                self.partie.jouer_carte(joueur, joueur.cartes[0])

        gagnant_pli = self.partie.terminer_pli("tout_atout")
        self.assertIsNotNone(gagnant_pli)

    def test_annoncer_enchere(self):
        joueur = self.joueurs[0]
        joueur2 = self.joueurs[1]
        joueur3 = self.joueurs[2]
        joueur4 = self.joueurs[3]

        enchere_valide = 100

        # Assurez-vous que l'enchère est acceptée
        self.partie.annoncer_enchere(joueur, enchere_valide, "coeur")
        self.assertEqual(self.partie._enchere, enchere_valide)

        # Assurez-vous qu'une enchère non valide déclenche une exception
        enchere_non_valide = 70
        with self.assertRaises(Exception):
            self.partie.annoncer_enchere(joueur, enchere_non_valide)

        # test sur lancement partie dans différentes conditions
        # Test 4 joueurs qui passe
        self.partie.ajouter_joueur(joueur)
        self.partie.ajouter_joueur(joueur2)
        self.partie.ajouter_joueur(joueur3)
        self.partie.ajouter_joueur(joueur4)
        self.partie.distribuer()
        self.partie._dico_enchere ={}
        deck_j1 = joueur.cartes
        self.partie.annoncer_enchere(joueur, 0, "passer")
        self.partie.annoncer_enchere(joueur2, 0, "passer")
        self.partie.annoncer_enchere(joueur3, 0, "passer")
        self.partie.annoncer_enchere(joueur4, 0, "passer")
        self.assertFalse(deck_j1 == joueur.cartes)
        # Test 3 joueurs qui passe 1 qui fait l'enchere
        self.partie._dico_enchere = {}
        self.partie._enchere=0
        self.partie.annoncer_enchere(joueur, 80, "coeur")
        self.partie.annoncer_enchere(joueur2, 0, "passer")
        self.partie.annoncer_enchere(joueur3, 0, "passer")
        self.partie.annoncer_enchere(joueur4, 0, "passer")
        self.assertTrue(self.partie.get_statut() == "JEU")
        # Test aucun joueur qui passe
        self.partie._dico_enchere = {}
        self.partie._enchere = 0
        self.partie._statut = "attente"
        deck_j1 = joueur.cartes
        self.partie.annoncer_enchere(joueur, 80, "coeur")
        self.partie.annoncer_enchere(joueur2, 90, "pique")
        self.partie.annoncer_enchere(joueur3, 100, "trefle")
        self.partie.annoncer_enchere(joueur4, 110, "carreau")

        self.partie.annoncer_enchere(joueur, 120, "coeur")
        self.partie.annoncer_enchere(joueur2, 130, "pique")
        self.partie.annoncer_enchere(joueur3, 140, "trefle")
        self.partie.annoncer_enchere(joueur4, 160, "carreau")

        self.assertFalse(self.partie.get_statut() == "JEU")
        self.assertTrue(deck_j1 == joueur.cartes)

    def test_verifier_enchere(self):
        joueur = self.joueurs[0]

        # Testez une enchère valide
        enchere_valide = 100
        self.assertTrue(self.partie._verifier_enchere(joueur, enchere_valide))

        # Testez une enchère non valide
        enchere_non_valide = 70
        self.assertFalse(self.partie._verifier_enchere(joueur, enchere_non_valide))


    def test_verifier_coup(self):
        joueur = self.joueurs[0]
        joueur2 = self.joueurs[1]
        partie = self.partie.creer_partie_4_joueur(self.joueurs)
        partie.distribuer()
        atout = "coeur"

        # test qu'un joueur ne puisse pas jouer 2 fois dans le meme pli
        partie._pli_actuelle[joueur] = Carte("coeur", 11)
        self.assertFalse(partie._verifier_coup(joueur, joueur.cartes[0], atout))
        partie._pli_actuelle = {}

        # test qu'une carte du joueur peut être jouée
        carte_valide = joueur.cartes[0]
        self.assertTrue(partie._verifier_coup(joueur, carte_valide, atout))

        # test qu'une carte non détenue par le joueur ne peut pas être jouée
        carte_non_valide = joueur2.cartes[0]
        self.assertFalse(partie._verifier_coup(joueur, carte_non_valide, atout))

        # test qu'une carte d'une famille différente ne peut être jouée que si le joueur n'a pas de carte de la famille actuelle
        partie._pli_actuelle = {}
        joueur2.cartes.append(Carte("coeur", 5))
        carte_test = Carte("pique", 10)
        joueur2.cartes.append(carte_test)
        partie._pli_actuelle [joueur] = Carte("coeur", 7)
        self.assertFalse(partie._verifier_coup(joueur, carte_test, atout))

        # test de si le joueur joue un atout moins fort que la meilleur carte du pli alors que c'est demandé atout et qu'il a un atout plus fort dans son deck
        partie._pli_actuelle = {}
        premiere_carte_pli = Carte("coeur", 10)
        joueur.cartes.append(premiere_carte_pli)
        partie.jouer_carte(joueur, premiere_carte_pli)
        atout_carte_moins = Carte("coeur", 7)
        atout_carte_plus = Carte("coeur", 11)
        joueur2.cartes.append(atout_carte_moins)
        joueur2.cartes.append(atout_carte_plus)
        self.assertFalse(partie._verifier_coup(joueur2, atout_carte_moins, "coeur"))


    def test_terminer_partie(self):
        partie = PartieCoinche()

        # Simulation d'une partie avec des points pour chaque équipe
        partie._pts_equipe_1 = 120
        partie._pts_equipe_2 = 80

        equipe1_joueur1 = Joueur(1)
        equipe1_joueur2 = Joueur(2)
        equipe2_joueur1 = Joueur(3)
        equipe2_joueur2 = Joueur(4)

        partie._equipe1 = [equipe1_joueur1, equipe1_joueur2]
        partie._equipe2 = [equipe2_joueur1, equipe2_joueur2]

        historique = partie.terminer_partie()
        self.assertIsInstance(historique, HistoriquePartie)
        self.assertEqual(historique.date, date.today())
        self.assertEqual(historique.nb_points_gagnants, 120)
        self.assertEqual(historique.nb_points_perdants, 80)
        self.assertEqual(historique.perdants_1, partie._equipe2[0])
        self.assertEqual(historique.perdants_2, partie._equipe2[1])
        self.assertEqual(historique.gagnants_1, partie._equipe1[0])
        self.assertEqual(historique.gagnants_2, partie._equipe1[1])


if __name__ == '__main__':
    unittest.main()


